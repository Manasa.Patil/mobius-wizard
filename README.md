    npm commands
    "start": start react app,
    "build": build react app,
    "electron:serve": script to start your React development server and Electron simultaneously. Your React application will be served at http://localhost:3000, and Electron will open with your React component,
    "electron:build": script to build your React application and package it as an Electron application, executable file created in dist folder

You can open dev tools for desktop app by CRT + SHIFT + I
