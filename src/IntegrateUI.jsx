import React, { useState } from 'react'
import { manipulateTiltFile } from './tiltConfig'
import { tiltNamespaceIsValid } from './util'
import { Button, Form } from 'react-bootstrap'
import { ui_modules } from './tiltConfig'
const { ipcRenderer, shell } = window.require('electron')

export const openLink = (e, link) => {
  e.stopPropagation();
  e.preventDefault();
  shell.openExternal(link);
}

function IntegrateUI(props) {
  const [tiltNamespace, setTiltNamespace] = props.tiltNamespaceHook

  const [miniservicesPath, setMiniservicesPath] = useState('')
  const [selectedUIModules, setSelectedUIModules] = useState([])
  const [tiltedUp, setTiltedUp] = useState(false)
  const miniservicePathResult = 'miniservice-path-result'

  React.useEffect(() => {
    ipcRenderer.on(miniservicePathResult, (event, data) => {
      setMiniservicesPath(data)
    })

    return () => {
      ipcRenderer.removeAllListeners()
    }
  }, [props])

  const onUIModulesChange = ({ target: { value } }) => {
    {
      if (selectedUIModules.includes(value)) {
        setSelectedUIModules(prevState =>
          prevState.filter(word => word != value)
        )
      } else {
        setSelectedUIModules(prevState => [...prevState, value])
      }
    }
  }
  const handleIntegrateWithUI = event => {
    openLink(event, 'http://localhost:10350')

    handleIntegrateWithUIWithoutCloning()
  }

  const trimTiltNamespace = fullTiltNamespace => {
    const result = fullTiltNamespace.split('.')[0].split('//')[1]
    return result
  }

  const handleIntegrateWithUIWithoutCloning = () => {
    setTiltedUp(true)

    manipulateTiltFile(miniservicesPath + `\\Tiltfile`, selectedUIModules)

    const resourceNames = ui_modules
      .filter(ui_module => selectedUIModules.includes(ui_module.name))
      .map(ui_module => ui_module.tiltResource)

    ipcRenderer.send(
      'execute-git-command',
      `cd ${miniservicesPath} && tilt up epic-base-app-ui ${resourceNames.join(
        ' '
      )} `
    )
  }
  const handleTiltDown = () => {
    setTiltedUp(false)
    ipcRenderer.send(
      'tilt-down',
      `cd ${miniservicesPath} && tilt down && npx kill-port 10350 && kubectl get services -o name -n  ${trimTiltNamespace(
        tiltNamespace
      )} | xargs -I % kubectl delete % -n ${trimTiltNamespace(
        tiltNamespace
      )} && kubectl get deployments -o name -n  ${trimTiltNamespace(
        tiltNamespace
      )} | xargs -I % kubectl delete % -n ${trimTiltNamespace(tiltNamespace)}`
    )
  }

  return (
    <Form onSubmit={handleIntegrateWithUI}>
      <Form.Group>
        <div
          className="alert alert-success"
          role="alert"
          style={{ width: '790px' }}
        >
          {/* primary or success */}
          <small
            id="emailHelp"
            className="form-text text-muted"
            style={{ marginTop: '1px', marginBottom: '1px' }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="15"
              fill="currentColor"
              className="bi bi-exclamation-triangle-fill flex-shrink-0 me-2"
              viewBox="0 0 15 15"
              role="img"
              aria-label="Warning:"
            >
              <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </svg>
            Pre-requisite
            <ul>
              <li>
                You have{' '}
                <a
                  href=""
                  className="link-warning"
                  onClick={(e) =>
                    openLink(e,
                      'https://appliedsystems.atlassian.net/wiki/spaces/EPIC/pages/912589087/How+to+use+Tilt+and+your+personal+namespace+for+Epic+development'
                    )
                  }
                >
                  completed Tilt setup
                </a>{' '}
                and miniservices-manifests is in the same path as the other ui-*
                and modules-* repos.
              </li>
              <li>
                You have checked out to appropriate branch and built the UI
                project
              </li>
            </ul>
          </small>
        </div>
      </Form.Group>
      <Form.Group>
        <small
          id="emailHelp"
          className="form-text text-muted"
          style={{ marginTop: '0', marginBottom: '5px' }}
        ></small>
        <br />
      </Form.Group>
      <div className="button-and-textfield">
        <Form.Group className="folder-textfield">
          <Form.Floating>
            <Form.Control
              type="text"
              id="miniservicesPath"
              className="git-settings-textbox"
              value={miniservicesPath}
              onChange={e => setMiniservicesPath(e.target.value)}
            />
            <label htmlFor="miniservicesPath">Miniservice-manifest path:</label>
          </Form.Floating>
        </Form.Group>
        <Button
          onClick={() => {
            ipcRenderer.send('select-directory', miniservicePathResult)
          }}
          variant="success"
          className="right-button"
        >
          Browse
        </Button>
      </div>
      <Form.Group>
        <br />
        {ui_modules.map(module => (
          <Form.Check
            key={module.name}
            inline // prettier-ignore
            type={'checkbox'}
            id={`default`}
            label={module.name}
            value={module.name}
            onChange={onUIModulesChange}
          />
        ))}
      </Form.Group>
      <br />
      <Form.Group>
        <Form.Floating>
          <Form.Control
            type="Text"
            value={tiltNamespace}
            onChange={e => {
              setTiltNamespace(e.target.value)
            }}
            className="custom-input git-settings-textbox"
          />
          <label htmlFor="tilt-namespace">
            Tilt Namespace Url (eg:
            https://apinney.us.development.appliedcloudplatform.com)
          </label>
        </Form.Floating>
      </Form.Group>
      <br />
      <Button
        variant="success"
        type="submit"
        className="custom-button"
        disabled={
          !miniservicesPath ||
          selectedUIModules.length == 0 ||
          !tiltNamespaceIsValid(tiltNamespace) ||
          tiltedUp
        }
      >
        Tilt Up
      </Button>
      {'   '}
      <Button
        variant="danger"
        className="custom-button"
        onClick={handleTiltDown}
        disabled={!miniservicesPath || !tiltNamespace || !tiltedUp}
      >
        Tilt Down
      </Button>
    </Form>
  )
}
export default IntegrateUI
