import { parse as tomlParse, stringify as tomlStringify } from 'smol-toml'
import { tiltNamespaceIsValid } from './util'
const fs = window.require('fs')
const path = window.require('path')

export const servers = [
  {
    name: 'Local'
  },
  {
    name: 'EpicD1A-US',
    host: '172.16.204.179'
  },
  {
    name: 'EpicD1A-UK',
    host: '172.16.204.181'
  },
  {
    name: 'AgileMeta',
    host: '172.16.204.134'
  },
  {
    name: 'EpicP1A',
    host: '172.16.204.201'
  },
  {
    name: 'E21R1-US',
    host: '172.16.204.172'
  },
  {
    name: 'VE201 (staging)',
    host: '10.105.4.39'
  },
  {
    name: 'VE001D1',
    host: '10.105.4.25'
  },
  {
    name: 'VE002D2',
    host: '10.105.4.14'
  }
]

export default function generateConfig(
  mobiusPath,
  useAmplitude,
  useDataDog,
  tiltNamespaceUrl,
  serverInfo
) {
  console.warn(useAmplitude)
  console.warn(useDataDog)
  console.warn(serverInfo)

  if (!mobiusPath) {
    console.error('Expected path for ui-mobius directory')
    return
  }

  // Verify our incoming path has `ui-mobius` on the end
  mobiusPath =
    path.basename(mobiusPath) === 'ui-mobius'
      ? mobiusPath
      : path.join(mobiusPath, 'ui-mobius')
  console.log(`mobiusPath: ${mobiusPath}`)
  const defaultTomlFilepath = path.join(mobiusPath, 'config.full.toml')
  const defaultTomlFile = fs.readFileSync(defaultTomlFilepath, 'utf8')
  let result = tomlParse(defaultTomlFile)

  if (useAmplitude) {
    result.web.productintelligence.enabled = true
    result.web.productintelligence.key = 'abfaa3b3a71724ef6191646af32f5131'
    result.web.productintelligence.pendoapikey = ''
  }

  if (useDataDog) {
    result.epic.datadogapikey = 'd6c05bbae2200afaec4d3f5e17ec7936'
    result.epic.datadogclienttoken = 'pub17859c1d4e13dbcf8674a01af2593a15'
    result.epic.datadogrumapplicationid = '5f60f47c-8a24-4d85-b0c3-ef6c894e86f6'
    result.epic.datadogrumclienttoken = 'pub115c912d388f4c7e955214b3bcfa607f'
  }

  if (tiltNamespaceIsValid(tiltNamespaceUrl)) {
    const hostPrefix = tiltNamespaceUrl.split('.')[0]
    console.log(`hostPrefix: "${hostPrefix}."`)
    result.epic.hybridhostprefix = `${hostPrefix}.`
  }

  if (!!serverInfo) {
    console.warn(`serverInfo: ${JSON.stringify(serverInfo)}`)
    switch (serverInfo.name) {
      case 'Local':
        result.epic.readconfig = true
        result.epic.readversion = true
        break

      // Staging servers
      case 'VE201':
      case 'VE001D1':
      case 'VE002D2':
        result.epic.hybridhost = 'us.staging.appliedcloudplatform.com'
        result.epic.licenseserviceurl =
          'https://licensing.us.staging.appliedcloudplatform.com'
        result.epic.iamv2url = 'https://iam-v2-uat.appliedcloudservices.com/'
        result.epic.sdsurl = 'https://default.appliedcloudplatformuat.com'
      // Fall through
      default:
        result.epic.readconfig = false
        result.epic.readversion = false
        result.epic.application.host = serverInfo.host
        result.epic.application.port = 8221
        result.epic.central.host = serverInfo.host
        result.epic.central.port = 8222
        break
    }
  }

  const outputFilepath = path.join(mobiusPath, 'config.toml')
  fs.writeFileSync(outputFilepath, tomlStringify(result))
}