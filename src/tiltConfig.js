const pathModule = window.require('path')
const { app } = window.require('@electron/remote')
const path = app.getPath('home')
const fs = window.require('fs')
const dockerString = "docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/"



export const ui_modules = [
    {
      name: 'accounting',
      gitName: 'ui-accounting',
      dockerString: `docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/ui-accounting', '../ui-accounting')`,
      tiltResource: 'epic-accounting-ui'
    },
    {
      name: 'activity',
      gitName: 'ui-activity',
      dockerString: `docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/ui-activity', '../ui-activity')`,
      tiltResource: 'epic-activity-ui'
    },
    {
      name: 'configure',
      gitName: 'ui-configure',
      dockerString: `docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/ui-configure', '../ui-configure')`,
      tiltResource: 'epic-configure-ui'
    },
    {
      name: 'crm',
      gitName: 'ui-crm',
      dockerString: `docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/ui-crm', '../ui-crm')`,
      tiltResource: 'epic-crm-ui'
    },
    {
      name: 'policy',
      gitName: 'ui-policy',
      dockerString: `docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/ui-policy', '../ui-policy')`,
      tiltResource: 'epic-policy-ui'
    },
    {
      name: 'procedures-interface',
      gitName: 'ui-procedures-interface',
      dockerString: `docker_build('registry.gitlab.com/appliedsystems/products/epic/user-interface/ui-procedures-interface', '../ui-procedures-interface')`,
      tiltResource: 'epic-procedures-interface-ui'
    },

  ];


  export const manipulateTiltFile = (filePath, selectedUIModules) => {
    
    fs.readFile(filePath, 'utf8', function (err,data) {
        if (err) {
          return console.log(err);
        }

        let changed = data;
        ui_modules.forEach((uiModule) => {
            const re = new RegExp('^.*' + uiModule.gitName + '.*', 'gm');
            changed = selectedUIModules.includes(uiModule.name) ? changed.replace(re, uiModule.dockerString) : changed.replace(re, '#' + uiModule.dockerString)
        })

        fs.writeFile(filePath, changed, 'utf8', function (err) {
            if (err) return console.log(err);
            });
        })
    
  }