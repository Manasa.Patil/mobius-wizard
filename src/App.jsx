import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './styles.css' // Import the custom CSS file for styling
import { Button, Form, Container, Card, Spinner } from 'react-bootstrap'
import generateConfig, { servers } from './mobiusConfig'
import GitSettings from './GitSettings'
import IntegrateUI from './IntegrateUI'
import { defaultTiltNamespace } from './util'

const { app } = window.require('@electron/remote')
const { ipcRenderer, shell } = window.require('electron')
const pathModule = window.require('path')

export const desktopPath = pathModule.join(app.getPath('home'), 'Desktop')

const openLink = (e, link) => {
  e.stopPropagation();
  e.preventDefault();
  shell.openExternal(link);
}

function App() {
  const path = app.getPath('home')
  const desktopPath = pathModule.join(path, 'Desktop')
  const mobiusCloneUrl =
    'https://gitlab.com/appliedsystems/products/epic/user-interface/ui-mobius.git'
  const mobiusRepoName = mobiusCloneUrl.split('.git')[0].split('/').pop()
  let isMac = false;
  const platform = navigator.userAgent.toLowerCase();
  if (platform && platform.includes('mac')) {
    isMac = true;
  }

  const [branch, setBranch] = useState('')
  const [selectedServer, setSelectedServer] = useState('Local')
  const [loadingMessage, setLoadingMessage] = useState('')
  const [isCloned, setIsCloned] = useState(false)
  const [mobiusPath, setMobiusPath] = useState('')
  const [gitResult, setGitResult] = useState('')
  const [isGitSettingsOpen, setGitSettingsOpen] = useState(true)
  const [isMobiusCustomizationOpen, setMobiusCustomizationOpen] = useState(true)
  const [isIntegrateWithUIOpen, setIntegrateWithUIOpen] = useState(true)
  const [isServerCustomizationOpen, setServerCustomizationOpen] = useState(true)
  const [useAmplitude, setUseAmplitude] = React.useState(false)
  const [useDataDog, setUseDataDog] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [shouldBuildInterface, setShouldBuildInterface] = useState(true)
  const [tiltNamespace, setTiltNamespace] = useState(defaultTiltNamespace)
  const startMobiusLocalDevFilename = 'Start-MobiusLocalDev.ps1'

  const executeCommand = 'execute-command'
  const npmInstall = 'npm-install'
  const buildInterface = 'build-interface'
  const runMobiusGo = 'run-mobius-go'
  const mobiusGoResult = 'mobius-go-result'
  const closeMobiusResult = 'mobius-close-result'
  const mobiusClose = 'mobius-close'


  const launchMobius = e => {
    e.preventDefault()
    e.stopPropagation()

    setIsLoading(true)
    generateConfig(
      mobiusPath,
      useAmplitude,
      useDataDog,
      tiltNamespace,
      selectedServer
    )
    setLoadingMessage(`Starting the proxy...`)
    const startMobiusLocalDevPath = pathModule.join(
      mobiusPath,
      'development',
      'ssl-host'
    )
    let runScript = `powershell.exe -ExecutionPolicy Bypass -File ${startMobiusLocalDevFilename}`
    if (isMac) {
      runScript = `pwsh -ExecutionPolicy Bypass -File ${startMobiusLocalDevFilename}`;
    }
    ipcRenderer.send(
      executeCommand,
      `cd ${startMobiusLocalDevPath} && ${runScript}`,
      npmInstall
    )
  }

  const handleMobiusClose = () => {
    ipcRenderer.send(
      executeCommand,
      `cd ${mobiusPath} && npx kill-port 8126 && docker stop mobius-ssl-local && docker rm -f mobius-ssl-local`,
      closeMobiusResult)

      setLoadingMessage(`Mobius closed`)
  }

  React.useEffect(() => {
    ipcRenderer.on(npmInstall, (event, data) => {
      console.log(data)
      setLoadingMessage('Installing dependencies...')
      const interfacePath = pathModule.join(mobiusPath, 'interface')
      const reactAdsPath = pathModule.join(interfacePath, 'react-ads')
      ipcRenderer.send(
        executeCommand,
        `cd ${reactAdsPath} && npm install && cd .. && npm install`,
        shouldBuildInterface ? buildInterface : runMobiusGo
      )
    })

    ipcRenderer.on(buildInterface, (event, data) => {
      console.log(data)
      setLoadingMessage('Building...')
      const interfacePath = pathModule.join(mobiusPath, 'interface');
      ipcRenderer.send(
        executeCommand,
        `cd ${interfacePath} && npm run ps:build`,
        runMobiusGo
      )
    })

    ipcRenderer.on(runMobiusGo, (event, data) => {
      console.log(data)
      setLoadingMessage(`Running the server`)
      setIsLoading(false)
      ipcRenderer.send('open-mobius')
            ipcRenderer.send(
        executeCommand,
        `cd ${mobiusPath} && go run mobius.go`,
        mobiusGoResult
      )
    })

    ipcRenderer.on(mobiusGoResult, (event, data) => {
      setIsLoading(false)
      setLoadingMessage('')
    })

    ipcRenderer.on(mobiusClose, (event, data) => {

    })

    return () => {
      ipcRenderer.removeAllListeners()
    }
  }, [launchMobius])



  return (
    <Container>
      <h1 className="mt-4 custom-heading">Mobius Wizard</h1>{' '}
      <div
        className="alert alert-success"
        role="alert"
        style={{
          width: '450px',
          alignItems: 'start',
          marginBottom: '30px'
        }}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="32"
          height="15"
          fill="currentColor"
          className="bi bi-exclamation-triangle-fill flex-shrink-0 me-2"
          viewBox="0 0 15 15"
          role="img"
          aria-label="Warning:"
        >
          <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
        </svg>
        <small id="emailHelp" className="form-text text-muted">
          Pre-requisite: You should be connected to VPN all the time
        </small>
      </div>
      <Card className="custom-card">
        {' '}
        <Card.Header>
          <Card.Title>
            <Button
              className="custom-primary-button"
              variant="dark"
              onClick={() => setGitSettingsOpen(!isGitSettingsOpen)}
              aria-controls="git-settings"
              aria-expanded={isGitSettingsOpen}
            >
              {isGitSettingsOpen ? ' Git Settings' : ' Git Settings'}
            </Button>
          </Card.Title>
        </Card.Header>
        {isGitSettingsOpen && (
          <Card.Body>
            <GitSettings
              mobiusPath={mobiusPath}
              onMobiusPathChange={newPath => setMobiusPath(newPath)}
            ></GitSettings>
          </Card.Body>
        )}
      </Card>
      <br />
      <Card className="my-2 custom-card">
        <Card.Header>
          <Card.Title>
            <Button
              className="custom-primary-button"
              onClick={() =>
                setMobiusCustomizationOpen(!isMobiusCustomizationOpen)
              }
              aria-controls="mobius-customization"
              aria-expanded={isMobiusCustomizationOpen}
            >
              {isMobiusCustomizationOpen
                ? ' Mobius Customization'
                : ' Mobius Customization'}
            </Button>
          </Card.Title>
        </Card.Header>
        {isMobiusCustomizationOpen && (
          <Card.Body>
            <Form.Check
              type="switch"
              onChange={e => setUseAmplitude(e.target.checked)}
              label={
                <span>
                  Amplitude{' '}
                  <a
                    href=""
                    className="small"
                    onClick={(e) => openLink(e, 'https://app.amplitude.com/analytics/AppliedSystems/activity')}
                  >
                    Link
                  </a>
                </span>
              }
            />
            <Form.Check
              type="switch"
              onChange={e => setUseDataDog(e.target.checked)}
              label={
                <span>
                  Data Dog RUM{' '}
                  <a
                    href=""
                    className="small"
                    onClick={(e) =>
                      openLink(e,
                        'https://asi-development.datadoghq.com/rum/sessions?query=%40type%3Asession&cols=&from_ts=1687373683243&to_ts=1687460083243&live=true'
                      )
                    }
                  >
                    Link
                  </a>
                </span>
              }
            />
            {(useAmplitude || useDataDog) && (
              <div
                className="alert alert-danger"
                role="alert"
                style={{
                  width: '600px',
                  height: '100px',
                  alignItems: 'start',
                  marginBottom: '30px'
                }}
              >
                <small id="emailHelp" className="form-text text-muted">
                  Note:
                  <ul>
                    <li>
                      For Data Dog RUM, you should have a user created{' '}
                      <a
                        href=""
                        className="link-danger"
                        onClick={(e) =>
                          openLink(e,
                            'https://gitlab.com/appliedsystems/enterprise/services/service-configuration/-/blob/master/configuration/development/DevOps/E/EPICD1A-US-DEVINT.yaml#L211'
                          )
                        }
                      >
                        here
                      </a>{' '}
                    </li>
                    <li>
                      Make sure you turn off these settings after use as both
                      the services are chargeable
                    </li>
                  </ul>
                </small>
              </div>
            )}
          </Card.Body>
        )}
      </Card>
      <br />
      <Card className="custom-card">
        <Card.Header>
          <Card.Title>
            <Button
              className="custom-primary-button"
              onClick={() =>
                setServerCustomizationOpen(!isServerCustomizationOpen)
              }
              aria-controls="server-customization"
              aria-expanded={isServerCustomizationOpen}
            >
              {isServerCustomizationOpen
                ? ' Server Customization'
                : ' Server Customization'}
            </Button>
          </Card.Title>
        </Card.Header>
        {isServerCustomizationOpen && (
          <Card.Body>
            <Form>
              <Form.Group controlId="server">
                <Form.Floating>
                  <Form.Control
                    id="select-server"
                    style={{ width: '300px', height: '40px' }}
                    className="custom-select"
                    as="select"
                    //defaultValue={servers[0].name}
                    onChange={e =>
                      setSelectedServer(
                        servers[
                          servers
                            .map(({ name }) => name)
                            .indexOf(e.target.value)
                        ]
                      )
                    }
                  >
                    <option>Select a server</option>
                    {servers.map(server => {
                      return (
                        <option value={server.name} key={server.name}>
                          {server.name}
                        </option>
                      )
                    })}
                  </Form.Control>
                  <label htmlFor="select-server">Server:</label>
                </Form.Floating>
              </Form.Group>
            </Form>
          </Card.Body>
        )}
      </Card>
      <br />
      <Card className="my-2 custom-card">
        <Card.Header>
          <Card.Title>
            <Button
              className="custom-primary-button"
              onClick={() => setIntegrateWithUIOpen(!isIntegrateWithUIOpen)}
              aria-controls="Integrate with UI repos"
              aria-expanded={isIntegrateWithUIOpen}
            >
              {isIntegrateWithUIOpen
                ? ' Integrate with UI'
                : ' Integrate with UI'}
            </Button>
          </Card.Title>
        </Card.Header>
        {isIntegrateWithUIOpen && (
          <Card.Body>
            <IntegrateUI
              tiltNamespaceHook={[tiltNamespace, setTiltNamespace]}
            ></IntegrateUI>
          </Card.Body>
        )}
      </Card>
      <br />
      <Form>
        <Form.Check
          type="switch"
          className="message"
          onChange={e => setShouldBuildInterface(e.target.checked)}
          checked={shouldBuildInterface}
          label="Build Interface"
        />
        <div className='button-container'>
          <Button
            onClick={launchMobius}
            variant="primary"
            className="mt-3 custom-button custom-launch-button"
          >
            {!isLoading && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="17"
                height="17"
                fill="currentColor"
                className="bi bi-rocket"
                viewBox="0 0 17 17"
              >
                <path d="M8 8c.828 0 1.5-.895 1.5-2S8.828 4 8 4s-1.5.895-1.5 2S7.172 8 8 8Z" />
                <path d="M11.953 8.81c-.195-3.388-.968-5.507-1.777-6.819C9.707 1.233 9.23.751 8.857.454a3.495 3.495 0 0 0-.463-.315A2.19 2.19 0 0 0 8.25.064.546.546 0 0 0 8 0a.549.549 0 0 0-.266.073 2.312 2.312 0 0 0-.142.08 3.67 3.67 0 0 0-.459.33c-.37.308-.844.803-1.31 1.57-.805 1.322-1.577 3.433-1.774 6.756l-1.497 1.826-.004.005A2.5 2.5 0 0 0 2 12.202V15.5a.5.5 0 0 0 .9.3l1.125-1.5c.166-.222.42-.4.752-.57.214-.108.414-.192.625-.281l.198-.084c.7.428 1.55.635 2.4.635.85 0 1.7-.207 2.4-.635.067.03.132.056.196.083.213.09.413.174.627.282.332.17.586.348.752.57l1.125 1.5a.5.5 0 0 0 .9-.3v-3.298a2.5 2.5 0 0 0-.548-1.562l-1.499-1.83ZM12 10.445v.055c0 .866-.284 1.585-.75 2.14.146.064.292.13.425.199.39.197.8.46 1.1.86L13 14v-1.798a1.5 1.5 0 0 0-.327-.935L12 10.445ZM4.75 12.64C4.284 12.085 4 11.366 4 10.5v-.054l-.673.82a1.5 1.5 0 0 0-.327.936V14l.225-.3c.3-.4.71-.664 1.1-.861.133-.068.279-.135.425-.199ZM8.009 1.073c.063.04.14.094.226.163.284.226.683.621 1.09 1.28C10.137 3.836 11 6.237 11 10.5c0 .858-.374 1.48-.943 1.893C9.517 12.786 8.781 13 8 13c-.781 0-1.517-.214-2.057-.607C5.373 11.979 5 11.358 5 10.5c0-4.182.86-6.586 1.677-7.928.409-.67.81-1.082 1.096-1.32.09-.076.17-.135.236-.18Z" />
                <path d="M9.479 14.361c-.48.093-.98.139-1.479.139-.5 0-.999-.046-1.479-.139L7.6 15.8a.5.5 0 0 0 .8 0l1.079-1.439Z" />
              </svg>
            )}
            {isLoading && <Spinner size="sm" role="status" className="spinner" />}{' '}
            Launch Mobius
          </Button>
          <Button
            variant="danger"
            className='mt-3 custom-button right-button'
            onClick={handleMobiusClose}
          >
            Shut Down Mobius
          </Button>
        </div>
        {loadingMessage && <span className="message">{loadingMessage}</span>}
      </Form>
      <br />
      <br />
      <br />
    </Container>
  )
}

export default App
