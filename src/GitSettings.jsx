import React, { useState } from 'react'
import { Button, Form, Container } from 'react-bootstrap'
const { ipcRenderer } = window.require('electron')
const pathModule = window.require('path')

function GitSettings(props) {
  const [gitResult, setGitResult] = useState(null)
  const [branch, setBranch] = useState('')
  const [showResultLoading, setShowResultLoading] = useState(false)
  const [branchResult, setBranchResult] = React.useState(null)
  const cloneUrl =
    'https://gitlab.com/appliedsystems/products/epic/user-interface/ui-mobius.git'
  const repoName = cloneUrl.split('.git')[0].split('/').pop()
  let isMac = false;
  const platform = navigator.userAgent.toLowerCase();
  if (platform && platform.includes('mac')) {
    isMac = true;
  }

  const isClonedResult = 'is-cloned-result';
  const mobiusPathResult = 'mobius-path-result';

  const handleOnMobiusPathChange = e => {
    props.onMobiusPathChange(e.target.value)
  }

  React.useEffect(() => {
    ipcRenderer.on(isClonedResult, (event, data) => {
      const isCloned = data.result && data.result.trim().toLowerCase() === 'true';
      handleCheckout(isCloned);
    });

    ipcRenderer.on('git-command-result', (event, data) => {
      setShowResultLoading(false)
      if (data.error) {
                setBranchResult(
          <div>
            Failed to check out branch: <code>{branch}</code>
          </div>
        )
      } else {
        setBranchResult(
          <div>
            Checked out to branch: <code>{branch}</code>
          </div>
        )
      }
    })

    ipcRenderer.on(mobiusPathResult, (event, data) => {
      props.onMobiusPathChange(data);
    });

    return () => {
      ipcRenderer.removeAllListeners()
    }
  }, [branch, props])

  const checkIfAlreadyCloned = () => {
    setShowResultLoading(true);
    let powershellCommand = 'powershell.exe';
    if (isMac) {
      powershellCommand = 'pwsh';
    }
    if (props.mobiusPath.includes('ui-mobius')) {
      ipcRenderer.send(
        'execute-command',
        `${powershellCommand} -Command Test-Path ${pathModule.join(props.mobiusPath)}`,
        isClonedResult
      )
    } else {
      ipcRenderer.send(
        'execute-command',
        `${powershellCommand} -Command Test-Path ${pathModule.join(props.mobiusPath, repoName)}`,
        isClonedResult
      )
    }
  }

  const handleCheckout = isCloned => {
    let gitCommand = ''
    let gitResult = ''
    const pathIncludingRepo = props.mobiusPath.includes('ui-mobius') ? props.mobiusPath : pathModule.join(props.mobiusPath, repoName);
    let pathNotIncludingRepo = props.mobiusPath;
    if (pathNotIncludingRepo.includes('ui-mobius')) {
      pathNotIncludingRepo = pathNotIncludingRepo.slice(0, -1 * ('ui-mobius'.length + 1));
    }

    if (isCloned) {
      gitCommand = `cd ${pathIncludingRepo} && git fetch && git checkout ${branch}`
      gitResult = (
        <div>
          <div>
            Mobius repository already exists at{' '}
            <code>{pathIncludingRepo}</code>
          </div>
        </div>
      )
    } else {
      gitCommand = `cd ${pathNotIncludingRepo} && git clone ${cloneUrl} && cd ${pathIncludingRepo} && git fetch && git checkout ${branch}`
      gitResult = (
        <div>
          <div>
            Cloned Mobius repository at{' '}
            <code>{pathModule.join(props.mobiusPath, repoName)}</code>
          </div>
        </div>
      )
      props.onMobiusPathChange(pathIncludingRepo)
    }

    ipcRenderer.send('execute-git-command', gitCommand);
    setGitResult(gitResult);
  }

  return (
    <Form>
      <div className='button-and-textfield'>
        <Form.Group className='folder-textfield'>
          <Form.Floating>
            <Form.Control
              type="text"
              id="mobiusPath"
              value={props.mobiusPath}
              onChange={handleOnMobiusPathChange}
              className="custom-input git-settings-textbox"
            />
            <label htmlFor="mobiusPath">
              Ui-mobius path (If Cloned: Enter Current Path, If Not Cloned: Enter
              Desired Path):
            </label>
          </Form.Floating>
        </Form.Group>
        <Button
          onClick={() => {
            ipcRenderer.send('select-directory', mobiusPathResult);
          }}
          variant="success"
          className='right-button'
        >Browse</Button>
      </div>
      <br />
      <Form.Group>
        <Form.Floating>
          <Form.Control
            type="text"
            id="branch"
            value={branch}
            onChange={e => setBranch(e.target.value)}
            className="custom-input git-settings-textbox"
          />
          <label htmlFor="branch">Git Branch (to be checked out to):</label>
        </Form.Floating>
      </Form.Group>
      <br />
      <Button
        variant="success"
        onClick={checkIfAlreadyCloned}
        disabled={!branch || !props.mobiusPath}
        className="custom-button"
      >
        Checkout
      </Button>
      <Container className="my-3">
        {showResultLoading ? (
          <>
            <div
              style={{ width: '20px', height: '20px' }}
              className="spinner-border text-success"
              role="status"
            >
              <span className="visually-hidden">Loading...</span>
            </div>
          </>
        ) : (
          (gitResult !== null || branchResult !== null) && (
            <>
              {gitResult && <div>{gitResult}</div>}
              {branchResult && <div>{branchResult}</div>}
            </>
          )
        )}
      </Container>
    </Form>
  )
}

export default GitSettings
