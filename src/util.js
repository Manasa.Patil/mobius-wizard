export const defaultTiltNamespace = 'https://<name>.<locale>.development.appliedcloudplatform.com';

export const tiltNamespaceIsValid = tiltNamespace => {
  return (
    !tiltNamespace.includes('<') &&
    !tiltNamespace.includes('>') &&
    ['.us.', '.ca.', '.uk.'].some(substr => tiltNamespace.includes(substr)) &&
    tiltNamespace.endsWith('development.appliedcloudplatform.com')
  )
  }