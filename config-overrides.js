const { override, addExternalBabelPlugins } = require('customize-cra')

module.exports = override(
  ...addExternalBabelPlugins(
    '@babel/plugin-proposal-nullish-coalescing-operator',
    '@babel/plugin-syntax-optional-chaining',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-transform-class-properties',
    'babel-plugin-transform-object-hasown'
  )
)
