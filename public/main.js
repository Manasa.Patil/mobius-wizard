const { app, BrowserWindow, ipcMain, shell, dialog } = require('electron')
const { exec } = require('child_process')
const path = require('path')
const isDev = require('electron-is-dev')

require('@electron/remote/main').initialize()

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1200,
    height: 1000,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true // in order to use remote, you need to set enableRemoteModule to true when creating your window in your main process
    }
  })

  win.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html')}`
  )
}

console.log(`${path.join(__dirname, '../build/index.html')}`)

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

ipcMain.on('execute-git-command', (event, command) => {
  exec(command, (error, stdout, stderr) => {
    if (error) {
      event.reply('git-command-result', { error: error.message })
    } else {
      event.reply('git-command-result', { result: stdout })
    }
  })
})

ipcMain.on('execute-command', (event, command, reply) => {
  exec(command, (error, stdout, stderr) => {
    if (error) {
      event.reply(reply, { error: error.message })
    } else {
      event.reply(reply, { result: stdout })
    }
  })
})

ipcMain.on('open-mobius', ()=>{
  require('electron').shell.openExternal('https://local.asi.corp/#/');
})

ipcMain.on('open-tilt', ()=>{
  require('electron').shell.openExternal('http://localhost:10350');
})

ipcMain.on('open-mobius-browser', ()=>{
  require('electron').shell.openExternal('https://local.asi.corp');
})


ipcMain.on('tilt-down', (event, command) => {
  exec(command, (error, stdout, stderr) => {
    if (error) {
      event.reply('tilt-down-result', { error: error.message })
    } else {
      event.reply('tilt-down-result', { result: stdout })
    }
  })
})


ipcMain.on('tilt-up', (event, command) => {
  exec(command, (error, stdout, stderr) => {
    if (error) {
      event.reply('tilt-up-result', { error: error.message })
    } else {
      event.reply('tilt-up-result', { result: stdout })
    }
  })
})

ipcMain.on('select-directory', (event, reply) => {
  const options = {
    properties: [ 'openDirectory']
  };

  dialog.showOpenDialog(options).then((result) => {
    if(!result.canceled) {
      const selectedDirectory = result.filePaths[0];
      event.reply(reply, selectedDirectory);
    }
  });
})
